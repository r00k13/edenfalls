﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PuzzleManager : MonoBehaviour
{
    public GameObject canvasMural;
    public ButtonPrompts buttonPrompts;

    private int selectedTile = -1;
    private bool[] tilesCollected = new bool[] { false, false, false, false, false, false, false };
    public Image[] tiles;
    public Image[] tileSlots;
    public Sprite[] tileImages;
    public Image imageSolved;

    void Start()
    {
        imageSolved.enabled = false;
    }

    void Update()
    {
        if (canvasMural.activeInHierarchy && GameManager.paused && InputManager.menu)         //if game is paused & player presses submit, hide canvas & resume play
        {
            GameManager.paused = false;
            canvasMural.SetActive(false);
            buttonPrompts.Hide();
        }
    }

    public void EditMural()
    {
        for (int i = 0; i < tiles.Length; i++)
        {
            tiles[i].gameObject.SetActive(tilesCollected[i]);
        }

        canvasMural.SetActive(true);
        GameManager.paused = true;
    }

    public void CollectTile(int id)
    {
        tilesCollected[id] = true;
        Debug.Log("Collected Tile " + id);
    }

    public void SelectTile(int id)
    {
        selectedTile = id;
    }

    public void PlaceTile(int id)
    {
        if(selectedTile > -1)
        {
            for(int i = 0; i < tileSlots.Length; i++)
            {
                if(tileSlots[i].sprite == tileImages[selectedTile])
                {
                    tileSlots[i].sprite = null;
                    break;
                }
            }
            tileSlots[id].sprite = tileImages[selectedTile];
            selectedTile = -1;

            if(tileSlots[0].sprite == tileImages[0] && tileSlots[1].sprite == tileImages[1] && tileSlots[2].sprite == tileImages[2] 
                && tileSlots[3].sprite == tileImages[3] && tileSlots[4].sprite == tileImages[4] && tileSlots[5].sprite == tileImages[5] 
                && tileSlots[6].sprite == tileImages[6])
            {
                imageSolved.enabled = true;
            }
        }
    }
}
