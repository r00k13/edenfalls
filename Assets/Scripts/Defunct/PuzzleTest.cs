using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleTest : MonoBehaviour
{
    public GameObject darnexPuzzleCanvas;
    private ConstellationNode[] stars;

    private void Start()
    {
        stars = darnexPuzzleCanvas.GetComponentsInChildren<ConstellationNode>();
    }

    public void CheckPuzzle()                               //Function called whenever a star is clicked
    {
        int correctStars = 0;                               //Integer to track how many stars are correct

        for (int i = 0; i < stars.Length; i++)               //Loop through the array of stars, one by one
        {
            if (stars[i].lit == stars[i].correct)            //If the star is lit & part of the hidden image, or unlit & not part of the hidden image
            {
                correctStars++;                             //Increase the number of correct stars
            }
            else                                            //Otherwise
            {
                break;                                      //Stop looping, as at least one of the stars is wrong
            }
        }

        if (correctStars == stars.Length)                    //If the number of correct stars is equal to the total number of stars
        {
            darnexPuzzleCanvas.GetComponent<Animator>().SetBool("Solved", true);
            StartCoroutine("CompleteStarPuzzle");           //Call the coroutine that completes the puzzle
        }
    }

    IEnumerator CompleteStarPuzzle()
    {
        yield return new WaitForSeconds(1.5f);
        //darnexPuzzleCanvas.SetActive(false);
        Debug.Log("Puzzle 4 completed");
        Application.Quit();
    }
}
