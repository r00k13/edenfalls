﻿using UnityEngine;
using System.Collections;

//destroys self after an interval

public class SelfDestruct : MonoBehaviour
{
    public float timer = 1.0f;                  //time until self destruction

	void OnEnable ()
    {
        Destroy(this.gameObject, timer);        //destroy in 'timer' seconds
    }
}
