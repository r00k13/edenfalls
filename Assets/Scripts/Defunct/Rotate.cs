﻿using UnityEngine;
using System.Collections;

//rotates an object around its axes

public class Rotate : MonoBehaviour
{
    public bool x = false;                                                          //bools to choose which axes to rotate around
    public bool y = false;
    public bool z = true;

    public float rotationSpeed = 75f;                                               //speed to rotate at

	void Update () 
	{
        if(x)
        {
            transform.Rotate(Vector3.left, rotationSpeed * Time.deltaTime);		    //rotate around x-axis
        }
        if (y)
        {
            transform.Rotate(Vector3.up, rotationSpeed * Time.deltaTime);		    //rotate around y-axis
        }
        if (z)
        {
            transform.Rotate(Vector3.forward, rotationSpeed * Time.deltaTime);		//rotate around z-axis
        }
    }
}
