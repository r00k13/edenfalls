using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

//Automatically selects the UI object it is attached to when enabled. Used when navigating menus with a gamepad.

public class DefaultButton : MonoBehaviour
{
    private void OnEnable()
    {
        if(/*InputManager.ps4 || InputManager.xbox*/Input.GetJoystickNames().Length > 0)
        {
            //GetComponent<Button>().Select();
            //EventSystem.current.SetSelectedGameObject(gameObject);
            StartCoroutine("SelectContinueButtonLater");
        }
    }

    IEnumerator SelectContinueButtonLater()
    {
        yield return null;
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(gameObject);
    }
}
