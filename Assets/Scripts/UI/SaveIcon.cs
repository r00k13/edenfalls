﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//When enabled, starts an animation and a coroutine to disable itself after 2 seconds.

public class SaveIcon : MonoBehaviour
{
    private Animator anim;

    private void OnEnable()
    {
        anim = GetComponent<Animator>();                    //Retrieve animator
        anim.SetBool("hover", true);                        //Start animation
        StartCoroutine("HideAfterDelay");
    }

    private IEnumerator HideAfterDelay()                    //Wait 2 seconds, then disable object
    {
        yield return new WaitForSeconds(2f);
        gameObject.SetActive(false);
    }
}
