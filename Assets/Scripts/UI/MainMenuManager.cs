using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Fungus;

//A set of functions used in the main menu, which are called by the fungus commands. Functions clear save data so  a new game can be started & show or hide the options menu.

public class MainMenuManager : MonoBehaviour
{
    public GameObject optionsCanvas;
    public Flowchart flowchart;

    public void ClearSaveData()                                 //Replace all data in the save file (or creates a new empty save file)                  
    {
        SaveData saveData = new SaveData();                     //Create save data representing a new game
        saveData.gameStage = 1;
        saveData.foundNotes = new bool[0];
        saveData.puzzleStage = 0;
        saveData.puzzleCollected = new bool[0];

        string jsonText = JsonUtility.ToJson(saveData);         //Assign save data
        File.WriteAllText(Application.persistentDataPath + "/" + "save.json", jsonText);
    }

    public void ShowOptions()                                   //Show the options menu
    {
        optionsCanvas.SetActive(true);
    }

    public void HideOptions()                                   //Hide the options menu & show the main menu again
    {
        optionsCanvas.SetActive(false);
        flowchart.ExecuteBlock("OpenMenu");
    }
}
