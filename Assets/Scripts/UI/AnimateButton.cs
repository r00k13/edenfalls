using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

//Sets the animation state of a button when it is selected or deselected.

public class AnimateButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, ISelectHandler, IDeselectHandler
{
    private Animator anim;

	void Start ()
    {
        anim = GetComponent<Animator>();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        anim.SetBool("hover", true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        anim.SetBool("hover", false);
    }

    public void OnSelect(BaseEventData eventData)
    {
        anim.SetBool("hover", true);
    }

    public void OnDeselect(BaseEventData eventData)
    {
        anim.SetBool("hover", false);
    }
}
