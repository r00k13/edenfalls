using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Template puzzle class, which other puzzles inherit from. Contains common functions such as automatically completing a puzzle.

public abstract class Puzzle : MonoBehaviour
{
    public abstract void AutoComplete();
    public abstract int GetStage();
    public abstract bool[] GetCollected();
    public abstract void SetStage(int stage, bool[] collected);
}
