using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//Handles the ending scene. Spawns Mr. Joab in when the player talks to Elyra, and enables a canvas on completion of the conversation.

public class Elyra : MonoBehaviour
{
    //public GameObject canvasEnd;
    public NPC joab;

    void Start ()
    {
        GameManager.gameStage = 10;                 //Make sure gamestage is 10, so correct conversation will be triggered
        GameManager.paused = false;
    }

    public void SpawnJoab()                         //Spawn Mr Joab & send him to Elyra
    {
        joab.gameObject.SetActive(true);
        joab.GoHome();
    }

    public void WinGame()
    {
        //canvasEnd.SetActive(true);                  //Enable canvas
        StartCoroutine("ReturnToTitle");
    }

    private IEnumerator ReturnToTitle()
    {
        yield return new WaitForSeconds(5f);
        SceneManager.LoadScene(0);
    }
}
