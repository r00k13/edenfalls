using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//First puzzle in the game, in which the player must collect pieces of a torn letter. Tracks how many pieces the player has found, 
//shows the appropriate pieces when collected using the Artifact Manager, and starts dialogues at the start and end of the puzzle using the Fungus Manager.

public class TornNote : Puzzle
{
    public Sprite[] tornPieces;                                             //Sprites of pieces of note
    public GameObject[] tornObjects;                                        //Note piece objects in world
    private int piecesFound = 0;                                            //Number of pieces the player has collected
    public Text piecesFoundText;

    private ArtifactManager artifactManager;                                
    private FungusManager fungusManager;
    private GameManager gameManager;
    public ButtonPrompts buttonPrompts;
    
    void Start ()
    {
        GameObject manager = GameObject.FindGameObjectWithTag("Manager");   //Retrieve manager scripts
        artifactManager = manager.GetComponent<ArtifactManager>();
        fungusManager = manager.GetComponent<FungusManager>();
        gameManager = manager.GetComponent<GameManager>();
    }

    public void ShowPiece(int i)
    {
        artifactManager.ShowSpecialNote(tornPieces[i]);                     //Display found piece
        if (i == 0)                                                         //If this is the first piece (the one given by Otis)
        {
            buttonPrompts.Hide();
            fungusManager.StartChat("TornNote", 1);                         //Start the first dialogue
            gameManager.NextStage();                                        
            for (int j = 0; j < tornObjects.Length; j++)                    //Enable the note objects in the world
            {
                tornObjects[j].SetActive(true);
            }
            piecesFoundText.gameObject.SetActive(true);                     //Enable the UI element
        }

        piecesFound++;                                                      //Increase the number of pieces found & update the UI element
        piecesFoundText.text = piecesFound + "/4";

        if (piecesFound == 4)                                               //If all pieces are found
        {
            buttonPrompts.Hide();
            fungusManager.StartChat("TornNote", 2);                         //Start second dialogue
            Debug.Log("Puzzle 1 completed");
            gameManager.NextStage();
            piecesFoundText.gameObject.SetActive(false);                    //Hide UI element
            Destroy(this);                                                  //Remove script from player
        }
    }

    void OnTriggerEnter(Collider collision)
    {
        if (enabled && collision.gameObject.tag == "TornNote")              //On entering trigger zone for note piece, show button prompts
        {
            buttonPrompts.Show();
        }
    }

    void OnTriggerStay(Collider collision)
    {
        if (enabled && InputManager.submit && !GameManager.paused)
        {
            if (collision.gameObject.tag == "TornNote")                     //If player clicks while inside trigger zone for note piece, collect note piece
            {
                ShowPiece(collision.gameObject.GetComponent<Artifact>().artifactID);
                Destroy(collision.gameObject);
            }
        }
     }

    void OnTriggerExit(Collider collision)
    {
        if (enabled && collision.gameObject.tag == "TornNote")              //On exiting trigger zone for note piece, hide button prompts
        {
            buttonPrompts.Hide();
        }
    }

    public override void AutoComplete()
    {
        for (int i = 0; i < tornObjects.Length; i++)                        //Destroy note pieces
        {
            Destroy(tornObjects[i]);
        }
        piecesFoundText.gameObject.SetActive(false);                        //Hide UI element
        Debug.Log("Puzzle 1 auto-completed");
        Destroy(this);                                                      //Remove script
    }

    public override int GetStage()
    {
        return piecesFound;                                                 //Return how many pieces have been found
    }

    public override bool[] GetCollected()                                   //Returns exactly which pieces have been collected
    {
        bool[] collected = new bool[tornObjects.Length];

        for(int i = 0; i < collected.Length; i++)
        {
            collected[i] = (tornObjects[i] == null);
        }

        return collected;
    }

    public override void SetStage(int stage, bool[] collected)                  
    {
        piecesFound = stage;                                                //Set number of pieces found

        if(piecesFound > 0)
        {
            for (int j = 0; j < collected.Length; j++)                      
            {
                if(collected[j])                                            //Destroy any pieces that have already been collected
                {
                    Destroy(tornObjects[j]);
                }
                else                                                        //Enable any pieces that have not been collected
                {
                    tornObjects[j].SetActive(true);
                }
            }
            piecesFoundText.gameObject.SetActive(true);                     //Enable & set UI element
            piecesFoundText.text = piecesFound + "/4";
        }
    }
}
