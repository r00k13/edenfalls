using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Third puzzle, in which the player must gain access to Otis� house and read a note. Tracks whether the player 
//has collected the axe, and destroys the door if the player interacts with it while holding the axe.

public class OtisHouse : Puzzle
{
    public GameObject otisDoor, axe, brokenDoor, otisNote;
    public Tile tileAxe;                                                                //Tile the axe is found on
    public ButtonPrompts buttonPrompts;
    private FungusManager fungusManager;
    private bool hasAxe = false;                                                        //Has the player collected the axe?
    public AudioClip doorBreakSound;

    void OnEnable ()
    {
        Destroy(GameObject.Find("Otis"));                                               //Removes Otis from the game
        otisDoor.tag = "PuzzleDoor";                                                    //Changes Otis' front door to a special one for the puzzle

        GameObject manager = GameObject.FindGameObjectWithTag("Manager");
        fungusManager = manager.GetComponent<FungusManager>();

        fungusManager.StartChat("OtisHouse", 1);                                        //Trigger dialogue to give player a hint
    }

    void OnTriggerEnter(Collider collision)
    {
        if (enabled && (collision.gameObject.tag == "Axe" || collision.gameObject.tag == "PuzzleDoor" || collision.gameObject.tag == "PuzzleNote")) //Show button prompts when player enters tagged trigger zone
        {
            buttonPrompts.Show();
        }
    }

    void OnTriggerStay(Collider collision)
    {
        if (enabled && (InputManager.submit && !GameManager.paused))                    //If player clicks while in trigger zone  
        {
            if (collision.gameObject.tag == "Axe")                                      
            {
                hasAxe = true;                                                          //Collect axe
                tileAxe.locked = false;                                                 //Unlock tile so TileManager can replace it
                Destroy(collision.gameObject);                                          //Destroy axe object
            }
            else if (collision.gameObject.tag == "PuzzleDoor")
            {
                if(hasAxe)                                                              //If player has axe, destroy door
                {
                    Destroy(otisDoor);
                    brokenDoor.SetActive(true);
                    GetComponent<SoundManager>().TriggerSound(doorBreakSound);          //Play door breaking sound
                }
                else                                                                    //Otherwise, start dialogue
                {
                    fungusManager.StartChat("OtisHouse", 2);
                }
            }
            else if (collision.gameObject.tag == "PuzzleNote")                      
            {
                GameObject manager = GameObject.FindGameObjectWithTag("Manager");
                manager.GetComponent<GameManager>().NextStage();                        //Complete puzzle
                manager.GetComponent<ArtifactManager>().ShowNote(5);
                Debug.Log("Puzzle 3 completed");
                Destroy(collision.gameObject);
                Destroy(this);                                                          //Remove script from player
            }
            buttonPrompts.Hide();
        }
    }

    void OnTriggerExit(Collider collision)
    {
        if (enabled && (collision.gameObject.tag == "Axe" || collision.gameObject.tag == "PuzzleDoor" || collision.gameObject.tag == "PuzzleNote")) //Hide button prompts when player leaves tagged trigger zone
        {
            buttonPrompts.Hide();
        }
    }

    public override void AutoComplete()
    {
        Destroy(GameObject.Find("Otis"));                                               //Destroy Otis, his door, his note, & the axe
        Destroy(otisDoor);
        brokenDoor.SetActive(true);
        Destroy(otisNote);
        Destroy(axe);
        tileAxe.locked = false;                                                         //Unlock axe tile
        Debug.Log("Puzzle 3 auto-completed");
        Destroy(this);                                                                  //Remove script from player
    }

    public override int GetStage()
    {
        if(!hasAxe)                                                                     //If player doesn't have axe
        {
            return 0;
        }
        else if(otisDoor == null)                                                       //If Otis' door has been destroyed                     
        {
            return 2;
        }
        else                                                                            //If player has axe, but hasn't destroyed door yet
        {
            return 1;
        }  
    }

    public override bool[] GetCollected()                                               //No objects to collect in this puzzle, so just return an empty array
    {
        bool[] collected = new bool[0];
        return collected;
    }

    public override void SetStage(int stage, bool[] collected)
    {
        Destroy(GameObject.Find("Otis"));                                               //Destroy Otis
        if(stage > 0)                                                                   //If player had collected axe
        {
            hasAxe = true;                                                              //Collect axe
            if(axe != null)
            {
                Destroy(axe);
            }
            tileAxe.locked = false;

            if (stage > 1)                                                              //If player had destroyed door
            {
                Destroy(otisDoor);
            }
        }
    }
}
