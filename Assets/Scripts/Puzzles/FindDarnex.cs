using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//The fourth puzzle. In this one, the player must find a character who only appears in the woods at night, solve a 2D mini-puzzle, 
//travel to a warped version of the town, and repair a mural. Listens for the Morning and Evening events to enable or disable the character, 
//tracks the player�s progress, uses the Digital Glitch package to alter the screen while in the warped town, and controls the mini-puzzle. 

public class FindDarnex : Puzzle
{
    public GameObject darnex, campfire, darnexPuzzleCanvas, puzzleInstructions, darnexNote, tileTownCreepy, creepyXander, missingPiece;
    public Tile darnexTile;                                                 //Tile Darnex is found on
    public ButtonPrompts buttonPrompts;
    private FungusManager fungusManager;
    private TileManager tileManager;
    private ConstellationNode[] stars;                                      //Stars in the constellation
    private bool hasMuralPiece = false;                                     //Has the player found the mural piece?

    void OnEnable ()
    {
        GameObject manager = GameObject.FindGameObjectWithTag("Manager");
        fungusManager = manager.GetComponent<FungusManager>();
        tileManager = manager.GetComponent<TileManager>();
        stars = darnexPuzzleCanvas.GetComponentsInChildren<ConstellationNode>();    //Get all stars in constellation

        if(TimeManager.clock >= 19f)                                        //If after 7pm, enable Darnex
        {
            SetDarnex();
        }
        else                                                                //Otherwise, enable Darnex's dead campfire trigger
        {
            SetCampfire();
        }

        TimeManager.Morning += SetCampfire;                                 //Add event listeners
        TimeManager.Nightfall += SetDarnex;
    }

    private void OnDisable()
    {
        TimeManager.Morning -= SetCampfire;                                 //Remove event listeners
        TimeManager.Nightfall -= SetDarnex;
    }

    void OnTriggerEnter(Collider collision)
    {
        if (enabled && (collision.gameObject.tag == "Darnex" || collision.gameObject.tag == "PuzzleNote" || collision.gameObject.tag == "Campfire" || collision.gameObject.tag == "Mural" || collision.gameObject.tag == "MuralTile"))  //Show button prompts when player enters tagged trigger zone
        {
            buttonPrompts.Show();
        }
    }

    void OnTriggerStay(Collider collision)
    {
        if (enabled && (InputManager.submit && !GameManager.paused))        //If player clicks while in trigger zone 
        {
            if (collision.gameObject.tag == "Darnex")                       
            {
                fungusManager.StartChat("Darnex");                          //Start the Darnex dialogue that leads into the puzzle
            }
            else if  (collision.gameObject.tag == "Campfire")
            {
                fungusManager.StartChat("FindDarnex", 1);                   //Start dialogue telling player to come back later
            }
            else if (collision.gameObject.tag == "PuzzleNote")
            {
                Destroy(collision.gameObject);                              //Destroy note object
                GameObject.FindGameObjectWithTag("Manager").GetComponent<ArtifactManager>().ShowNote(6);                    
                Destroy(tileManager.tilesMiddle[8].gameObject);             //Destroy existing tile in creepy town's place
                tileTownCreepy.SetActive(true);                             //Enable creepy town
                tileManager.tilesMiddle[8] = tileTownCreepy.transform;      //Add creepy town to array in TileManager
                darnexTile.locked = false;                                  //Unlock Darnex tile
            }
            else if (collision.gameObject.tag == "Mural")                   //If trigger zone is above creepy town mural                
            {
                if(hasMuralPiece)                                           //If player has the missing piece, repair the mural & enable Creepy Xander                                     
                {
                    missingPiece.SetActive(true);
                    creepyXander.SetActive(true);
                    Destroy(collision.gameObject);                          //Destroy trigger zone
                }
                else
                {
                    fungusManager.StartChat("FindDarnex", 2);               //Start dialogue pointing out mural
                }
            }
            else if (collision.gameObject.tag == "MuralTile")
            {
                fungusManager.StartChat("FindDarnex", 3);                   //Start dialogue suggesting the player repairs the mural    
                hasMuralPiece = true;                                       //Collect mural piece
                Destroy(collision.gameObject);
            }
            buttonPrompts.Hide();
        }
    }

    void OnTriggerExit(Collider collision)
    {
        if (enabled && (collision.gameObject.tag == "Darnex" || collision.gameObject.tag == "PuzzleNote" || collision.gameObject.tag == "Campfire" || collision.gameObject.tag == "Mural" || collision.gameObject.tag == "MuralTile"))  //Hide button prompts when player leaves tagged trigger zone
        {
            buttonPrompts.Hide();
        }
    }

    public void CompletePuzzle()
    {
        Debug.Log("Puzzle 4 completed");
        tileTownCreepy.GetComponent<Tile>().locked = false;                 //Unlock creepy town
        GameObject.FindGameObjectWithTag("Manager").GetComponent<GameManager>().NextStage();
        Destroy(this);                                                      //Remove script from player
    }

    private void SetDarnex()
    {
        if(darnex != null)                                                  //If Darnex still exists, enable him & disable the dead campfire trigger
        {
            darnex.SetActive(true);
            campfire.SetActive(false);
        }
    }

    private void SetCampfire()
    {
        if (darnex != null)                                                 //If Darnex still exists, disable him & enable the dead campfire trigger
        {
            darnex.SetActive(false);
            campfire.SetActive(true);
        }
    }

    public void ToggleInstructions()                                        //Show or hide the instructions item on the puzzle canvas
    {
        puzzleInstructions.SetActive(!puzzleInstructions.activeInHierarchy);
    }

    public void StartPuzzle()
    {
        darnexPuzzleCanvas.SetActive(true);                                 //Enable the puzzle canvas
        Cursor.visible = true;
    }

    public void CheckPuzzle()                                               //Function called whenever a star is clicked
    {
        int correctStars = 0;                                               //Integer to track how many stars are correct

        for(int i = 0; i < stars.Length; i++)                               //Loop through the array of stars, one by one
        {
            if(stars[i].lit == stars[i].correct)                            //If the star is lit & part of the hidden image, or unlit & not part of the hidden image
            {
                correctStars++;                                             //Increase the number of correct stars
            }
            else                                                        
            {
                break;                                                      //Stop looping, as at least one of the stars is wrong
            }
        }

        if(correctStars == stars.Length)                                    //If the number of correct stars is equal to the total number of stars
        {
            darnexPuzzleCanvas.GetComponent<Animator>().SetBool("Solved", true);
            StartCoroutine("CompleteStarPuzzle");                           //Call the coroutine that completes the puzzle
        }

    }

    IEnumerator CompleteStarPuzzle()
    {
        yield return new WaitForSeconds(1.5f);                              //Wait 1.5 seconds to allow time for animation

        darnexPuzzleCanvas.SetActive(false);                                //Disable canvas
        darnexNote.SetActive(true);
        Destroy(darnex);
        TimeManager.Morning -= SetCampfire;                                 //Remove event listeners
        TimeManager.Nightfall -= SetDarnex;
        Cursor.visible = false;
        GameManager.paused = false;
    }

    public override void AutoComplete()
    {
        Destroy(darnex);                                                    //Destoy puzzle objects
        Destroy(darnexNote);
        Destroy(tileTownCreepy);
        darnexTile.locked = false;                                          //Unlock Darnex tile
        Debug.Log("Puzzle 4 auto-completed");
        Destroy(this);                                                      //Remove script from player
    }

    public override int GetStage()
    {
        if(!enabled || darnex != null)                                      //If player hasn't found Darnex yet
        {
            return 0;
        }
        else if(darnexNote != null)                                         //If constellation puzzle has been solved, but player hasn't collected the note
        {
            return 1;
        }
        else if(!hasMuralPiece)                                             //If player hasn't found mural piece yet
        {
            return 2;
        }
        else if(creepyXander.activeInHierarchy)                             //If mural is fixed but player hasn't spoken to Creepy Xander
        {
            return 3;
        }
        else                                                                //Shouldn't ever happen
        {
            return 4;
        }
    }

    public override bool[] GetCollected()                                   //No objects to collect in this puzzle, so just return an empty array
    {
        bool[] collected = new bool[0];
        return collected;
    }

    public override void SetStage(int stage, bool[] collected)
    {
        if (stage > 0)                                                      //If constellation puzzle had been solved
        {
            Destroy(darnex);
            if(stage > 1)                                                   //If note had been collected
            {
                Destroy(tileManager.tilesMiddle[8].gameObject);             //Destroy existing tile in creepy town's place
                tileTownCreepy.SetActive(true);                             //Enable creepy town
                tileManager.tilesMiddle[8] = tileTownCreepy.transform;      //Add creepy town to array in TileManager
                darnexTile.locked = false;                                  //Unlock Darnex tile
                Destroy(darnexNote);

                if(stage > 2)                                               //If mural piece had been collected
                {
                    hasMuralPiece = true;

                    if(stage > 3)                                           //If mural had been fixed
                    {
                        creepyXander.SetActive(true);
                    }
                }
            }
        }
    }
}
