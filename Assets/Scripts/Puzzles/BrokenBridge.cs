using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Second puzzle in the game. The player must cross a river by collecting rocks and building a set of stepping stones. 
//Tracks which rocks have been collected and enables or disables the appropriate rocks and colliders in the river.
//When the puzzle is complete, warps the player and Mrs. Yce to her home.

public class BrokenBridge : Puzzle
{
    public ButtonPrompts buttonPrompts;
    private FungusManager fungusManager;

    public GameObject bridge, bridgeBroken;                             //Bridge models
    public GameObject yce, dummyYce;                                    //Mrs Yce & the dummy that is placed on the opposite side of the bridge

    private bool carrying = false;                                      //Is the player carrying a rock? 
    private int rocksPlaced = 0;                                        //How many rocks have been placed
    public GameObject[] rocks;                                          //Rocks in the river
    public GameObject[] rockSlots;                                      //Colliders in the river that block access until a rock is placed
    public GameObject[] puzzleRocks;                                    //Collectable rocks
    public GameObject splash;                                           //Splash particle effect prefab

    public Door[] yceDoors;                                             //Doors that will be unlocked when Mrs Yce is rescued

    void Start ()
    {
        GameObject manager = GameObject.FindGameObjectWithTag("Manager");
        fungusManager = manager.GetComponent<FungusManager>();
    }

    void OnEnable()
    {
        bridge.SetActive(false);                                        //Disable bridge, enable broken bridge & Mrs Yce
        bridgeBroken.SetActive(true);
        yce.SetActive(true);
    }

    void OnTriggerEnter(Collider collision)
    {
        if (enabled && (collision.gameObject.tag == "Rock" || collision.gameObject.tag == "RockSpace" || collision.gameObject.tag == "Yce"))    //Show button prompts when player enters tagged trigger zone
        {
            buttonPrompts.Show();
        }
    }

    void OnTriggerStay(Collider collision)
    {
        if (enabled && (InputManager.submit && !GameManager.paused))    //If player clicks while in trigger zone   
        {
            if (collision.gameObject.tag == "Rock")                     
            {
                if (carrying)                                           //If player is already carrying a rock, start dialogue 
                {
                    fungusManager.StartChat("BridgePuzzle", 2);
                }
                else                                                    //Otherwise, set carrying true & destroy rock
                {
                    carrying = true;
                    buttonPrompts.Hide();
                    Destroy(collision.gameObject);
                }  
            }
            else if (collision.gameObject.tag == "RockSpace")
            {
                if (carrying)                                           //If player is carrying a rock
                {
                    rocks[rocksPlaced].SetActive(true);                 //Place rock
                    Instantiate(splash, rocks[rocksPlaced].transform.position, splash.transform.rotation);  //Place splash prefab under rock
                    rockSlots[rocksPlaced].SetActive(false);            //Disable collder so player can walk forward to next space
                    buttonPrompts.Hide();
                    rocksPlaced++;
                    carrying = false;
                }
                else                                                    //Otherwise, start dialogue
                {
                    fungusManager.StartChat("BridgePuzzle", 1);
                }
            }
            else if (collision.gameObject.tag == "Yce")                 
            {
                fungusManager.StartChat("BridgePuzzle", 3);             //Start Mrs. Yce conversation
            }
        }
    }

    void OnTriggerExit(Collider collision)
    {
        if (enabled && (collision.gameObject.tag == "Rock" || collision.gameObject.tag == "RockSpace" || collision.gameObject.tag == "Yce"))    //Hide button prompts when player leaves tagged trigger zone
        {
            buttonPrompts.Hide();
        }
    }

    public void Complete()
    {
        Debug.Log("Puzzle 2 completed");
        GameObject.FindGameObjectWithTag("Manager").GetComponent<GameManager>().NextStage();

        SendYceHome();                                                  //Send Mrs. Yce to home location & unlock doors                                               

        transform.position = yce.transform.position + new Vector3(0, 1f, 0) + yce.transform.forward * 2;        //Move player in front of Mrs Yce
        GetComponent<PlayerManager>().SetMeshLook(yce.transform.position);  //Rotate player mesh to face Mrs. Yce

        Camera.main.transform.position = transform.position + new Vector3(0, 5, 0) + transform.forward * -5f;   //Move camera to near player
        Camera.main.transform.rotation = transform.rotation;

        Destroy(this);                                                  //Remove script from player
    }

    void SendYceHome()
    {
        yce.SetActive(true);
        yce.tag = "Character";                                          

        NPC yceScript = yce.GetComponent<NPC>();                        //Enable NPC script, allowing navigation, & use it to warp home
        yceScript.enabled = true;
        yceScript.WarpHome();

        for (int i = 0; i < yceDoors.Length; i++)                       //Unlock doors for Yce house & store
        {
            yceDoors[i].locked = false;
        }

        Destroy(dummyYce);                                              //Destroy dummy
    }

    public override void AutoComplete()
    {
        bridge.SetActive(false);                                        //Switch bridges
        bridgeBroken.SetActive(true);

        SetStage(rocks.Length, new bool[]{true, true, true});           //Enable stepping stones & destroy puzzle rocks
        SendYceHome();
        Debug.Log("Puzzle 2 auto-completed");
        Destroy(this);                                                  //Remove script
    }

    public override int GetStage()      
    {
        int stage = 0;  
        for (int i = 0; i < rocks.Length; i++)                          //Increase stage by 1 for each rock that has been enabled
        {
            if(rocks[i].activeInHierarchy)
            {
                stage++;
            }
        }
        return stage;
    }

    public override bool[] GetCollected()
    {
        bool[] collected = new bool[puzzleRocks.Length];

        for (int i = 0; i < collected.Length; i++)                      //For each rock that has been destroyed, set bool in array to true
        {
            collected[i] = (puzzleRocks[i] == null);
        }

        return collected;
    }

    public override void SetStage(int stage, bool[] collected)
    {
        if(stage != 0)                                                  //If at least one rock has been placed
        {
            for (int i = 1; i <= stage; i++)                            //Enable rocks & disable colliders
            {
                rocks[i - 1].SetActive(true);                           
                rockSlots[i - 1].SetActive(false);
            }
        }
        
        int totalCollected = 0;

        for (int j = 0; j < puzzleRocks.Length; j++)                      
        {
            if (collected[j])                                           //If rock has already been collected, destroy it
            {
                Destroy(puzzleRocks[j]);
                totalCollected++;
            }
            else                                                        //Otherwise, enable it
            {
                puzzleRocks[j].SetActive(true);                         
            }
        }

        if (totalCollected != stage)                                    //If more rocks have been collected than placed
        {
            carrying = true;
        }
    }

    
}
