using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Attached to lines between stars in the 2D mini-puzzle. Track the two stars they connect, and display an image if both are lit.

public class ConstellationLine : MonoBehaviour
{
    public ConstellationNode node1, node2;              //Stars the line connects
    private Image img;                                  //Image component of line

    private void Start()
    {
        img = GetComponent<Image>();                    //Retrieve the image
    }

    private void Update()
    {
        if(node1.lit && node2.lit)                      //If both stars are lit
        {
            img.enabled = true;                         //Enable the image
        }
        else                                            //Otherwise
        {
            img.enabled = false;                        //Disable the image
        }
    }
}
