using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Final puzzle in the game, in which the player must lure Mr Joab out of his house so they can steal the missing mural piece and use it 
//to repair the town mural. To do this, they must buy matches and set a tree on fire, but to obtain them they must steal money from a stash 
//hidden in a cabin in the woods. Tracks the player�s progress and enables Mr. Joab at the appropriate time.

public class FixDeer : Puzzle
{
    public GameObject muralCollider, fire, joabDoor, missingPiece;
    public Tile tileStash;                                              //Tile the hidden stash is found on
    public NPC joab;                                                    //Mr Joab
    public ButtonPrompts buttonPrompts;
    private GameManager gameManager;
    private FungusManager fungusManager;
    private bool hasMuralPiece = false;                                 //Has the player obtained the missing piece?

    void OnEnable()
    {
        GameObject manager = GameObject.FindGameObjectWithTag("Manager");
        gameManager = manager.GetComponent<GameManager>();
        fungusManager = manager.GetComponent<FungusManager>();

        muralCollider.SetActive(true);                                  //Enable trigger zone above mural
        joabDoor.tag = "PuzzleDoor";                                    //Change Joab door into a special door
    }

    void OnTriggerEnter(Collider collision)
    {
        if (enabled && (collision.gameObject.tag == "Stash" || collision.gameObject.tag == "PuzzleDoor" || collision.gameObject.tag == "PuzzleTree" || collision.gameObject.tag == "Mural" || collision.gameObject.tag == "MuralTile")) //Show button prompts when player enters tagged trigger zone
        {
            buttonPrompts.Show();
        }
    }

    void OnTriggerStay(Collider collision)
    {
        if (enabled && (InputManager.submit && !GameManager.paused))    //If player clicks while in trigger zone 
        {
            if (collision.gameObject.tag == "Stash")
            {
                fungusManager.SetFlowchartBool("hasMoney", true);       //Set variable in the flowchart
                fungusManager.StartChat("FixDeer", 4);                  //Start dialogue to say that money has been obtained
                tileStash.locked = false;                               //Unlock stash tile
                Destroy(collision.gameObject);                          //Destroy stash
            }
            else if (collision.gameObject.tag == "Mural")
            {
                if (hasMuralPiece)
                {
                    missingPiece.SetActive(true);                       //Enable missing piece of mural
                    gameManager.NextStage();                            //Go to next stage, which results in endgame scene being loaded 
                    Destroy(collision.gameObject);                      //Destroy trigger zone
                }
                else
                {
                    fungusManager.StartChat("FixDeer", 1);              //Start dialogue hinting that player should fix deer mural
                }
            }
            else if (collision.gameObject.tag == "PuzzleDoor")
            {
                fungusManager.StartChat("FixDeer", 2);                  //Start dialogue hinting that player should try to find another way in
            }
            else if (collision.gameObject.tag == "MuralTile")
            {
                fungusManager.StartChat("FixDeer", 5);                  //Start dialogue saying that player has found the missing piece
                hasMuralPiece = true;
                Destroy(collision.gameObject);                          //Destroy mural piece
            }
            else if (collision.gameObject.tag == "PuzzleTree")
            {
                switch(GameManager.gameStage)
                {
                    case 7:
                        fungusManager.StartChat("FixDeer", 3);          //Start dialogue hinting that player should burn the tree to attract attention
                        GameManager.gameStage++;
                        break;
                    case 8:
                        fungusManager.StartChat("FixDeer", 3);          //Start dialogue hinting that player should burn the tree to attract attention
                        break;
                    case 9:
                        fire.SetActive(true);                           //Enable fire on tree
                        joab.gameObject.SetActive(true);                //Enable Mr Joab
                        joab.GoHome();                                  //Send Mr Joab to the burning tree
                        joabDoor.tag = "Door";                          
                        Door door = joabDoor.GetComponent<Door>();      //Unlock & open Joab door
                        door.locked = false;
                        door.Open();
                        break;
                }
            }
            buttonPrompts.Hide();
        }
    }

    void OnTriggerExit(Collider collision)
    {
        if (enabled && (collision.gameObject.tag == "Stash" || collision.gameObject.tag == "PuzzleDoor" || collision.gameObject.tag == "PuzzleTree" || collision.gameObject.tag == "Mural" || collision.gameObject.tag == "MuralTile")) //Hide button prompts when player leaves tagged trigger zone
        {
            buttonPrompts.Hide();
        }
    }

    public void GetMatches()                                            //Called from fungus dialogue when player obtains matches
    {
        GameManager.gameStage = 9;
    }

    public override void AutoComplete()                                 //Not really necessary, as completing this puzzle results in immediately moving to the next scene
    {
        fungusManager.SetFlowchartBool("hasMoney", true);               //Set variable in the flowchart
        joab.gameObject.SetActive(true);                                //Enable Mr Joab
        Debug.Log("Puzzle 5 auto-completed");
        Destroy(this);                                                  //Remove script from player
    }

    public override int GetStage()
    {
        switch (GameManager.gameStage)
        {
            case 8:
                if(fungusManager.GetFlowchartBool("hasMoney"))          //If player has found stash
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            case 9:
                if(fire.activeInHierarchy)                              //If player has burned tree
                {
                    if(hasMuralPiece)                                   //If player has found mural piece
                    {
                        return 2;
                    }
                    else
                    {
                        return 1;
                    }
                }
                else
                {
                    return 2;
                }
            default:
                return 0;
        }
        
    }

    public override bool[] GetCollected()                               //No objects to collect in this puzzle, so just return an empty array
    {
        bool[] collected = new bool[0];
        return collected;
    }

    public override void SetStage(int stage, bool[] collected)
    {
        switch (GameManager.gameStage)
        {
            case 8:
                if(stage == 1)                                          //If player had found stash
                {
                    fungusManager.SetFlowchartBool("hasMoney", true);
                }
                break;
            case 9:
                if(stage > 0)                                           //If player had burned tree
                {
                    fire.SetActive(true);
                    joab.gameObject.SetActive(true);                    //Enable Mr Joab & send him to the tree
                    joab.GoHome();
                    joabDoor.tag = "Door";
                    Door door = joabDoor.GetComponent<Door>();          //Unlock & open Joab door
                    door.locked = false;
                    door.Open();

                    if(stage == 2)                                      //If player had collected mural tile
                    {
                        hasMuralPiece = true;
                        Destroy(GameObject.FindGameObjectWithTag("MuralTile"));
                    }
                }
                break;
        }
    }
}
