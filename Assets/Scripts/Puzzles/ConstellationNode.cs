using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Attached to stars in the 2D mini-puzzle. Contains a function which is called when the star is clicked on, which tracks whether the star is lit and toggles its opacity.

public class ConstellationNode : MonoBehaviour
{
    public bool lit = true;                                     //Is the star lit?
    public bool correct;                                        //Is the star part of the hidden image?

    public void ToggleNode()                                    //Function called when button is clicked
    {
        lit = !lit;                                             //Toggle the lit value
        Image img = GetComponent<Image>();                      //Get the star image
        Color c = img.color;                                    //Get the colour of the image

        if (lit)                                                //If the star is lit
        {
            c.a = 1f;                                           //Set the opacity to 100%
        }
        else                                                    //Otherwise
        {
            c.a = 0.3f;                                         //Set the opacity to 30%
        }

        img.color = c;                                          //Assign the colour to the star image
    }
}
