using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Sets its name & tag to match those of any NPC who walks into it, acting as a larger trigger zone for the NPC. Placed in areas such as shop counters where player may not be able to reach the NPC.

public class ShopCounter : MonoBehaviour
{
    private string defaultName;                                             //Default name of the object

    private void Start()
    {
        defaultName = gameObject.name;                                                  //Set default name
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Character" && gameObject.name == defaultName)  //If an NPC enters the trigger zone & it isn't already being used by another NPC
        {
            NPC charScript = collision.gameObject.GetComponent<NPC>();                  //Check that the NPC has an NPC script on it (I've already forgotten why this is necessary)
            if(charScript != null)
            {
                gameObject.tag = "Character";                                           //Set name & tag to match the NPC
                gameObject.name = collision.gameObject.name;
            }
        }
    }

    void OnTriggerExit(Collider collision)
    {
        if (collision.gameObject.tag == "Character" && collision.gameObject.name == gameObject.name)    //If the object that left is an NPC with the same name as this zone
        {
            gameObject.tag = "Untagged";                                                //Reset name & tag
            gameObject.name = defaultName;
        }
    }
}
