using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

//Listens for the Nightfall event, then follows the player if they are away from the town, triggering a conversation upon reaching them.

public class OtisRescue : MonoBehaviour
{
    private bool rescueTime = false;                                                                    //Set true when it's time to rescue the player
    private NavMeshAgent agent;
    private float updateTime = 0f;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();                                                           //Retrieve NavMeshAgent & set destination to current position
        agent.destination = transform.position;
    }

    void OnEnable()
    {
        TimeManager.Morning += StopSearching;                                                           //Add event listeners
        TimeManager.Nightfall += SearchForPlayer;                     
    }

    void OnDisable()
    {
        TimeManager.Morning -= StopSearching;                                                           //Remove event listeners
        TimeManager.Nightfall -= SearchForPlayer;                     
    }

    void Update()
    {
        if(rescueTime && Vector3.Distance(GameObject.FindGameObjectWithTag("Player").transform.position, Vector3.zero) > 70f && !GameManager.paused)       //If it's rescue time & the player is outside the village
        {
            if (Time.timeSinceLevelLoad > updateTime + 1f)                                              //If it's been at least 1 second since the destination was last updated
            {
                agent.destination = GameObject.FindGameObjectWithTag("Player").transform.position;      //Set destination to wherever the player currently is
                GetComponent<Animator>().SetBool("Moving", true);                                       //Set animation state to moving
                updateTime = Time.timeSinceLevelLoad;                                                   //Record time this destination update took place
            }

            if (Vector3.Distance(GameObject.FindGameObjectWithTag("Player").transform.position, transform.position) < agent.stoppingDistance + 2f)   //If closer to player than stopping distance (plus margin of error)
            {
                Animator animator = GetComponent<Animator>();
                animator.SetBool("Moving", false);                                                      //Set animation state to idle 
                GameObject.FindGameObjectWithTag("Manager").GetComponent<FungusManager>().StartChat("OtisRescue", 0);   //Start the OtisRescue conversation
                rescueTime = false;
                animator.SetBool("Talking", true);                                                      //Start talking animation
                GetComponent<NavMeshAgent>().destination = transform.position;                          //Set destination to current position
            }
        }
    }

    public void SearchForPlayer()                                                                       //Set state to searching in evening
    {
        rescueTime = true;
    }

    public void StopSearching()                                                                         //Reset state in morning so he's not walking on the spot or talking to himself
    {
        rescueTime = false;
        Animator animator = GetComponent<Animator>();
        animator.SetBool("Moving", false);
        animator.SetBool("Talking", false);
    }  
}
