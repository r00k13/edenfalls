using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

//Contains functions to move the object around using a NavMeshAgent when specific events occur. Also triggers animations when moving or talking.

public class NPC : MonoBehaviour
{
    private Transform target;                                                           //Current destination
    public Transform home, morning, afternoon, evening;                                 //Locations to travel to
    private NavMeshAgent agent;
    private Animator animator;
    private Door hitDoor;                                                               //Last door NPC has walked through

    void OnEnable()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();

        TimeManager.Morning += ResetPosition;                                           //Add event listeners
        TimeManager.Afternoon += GoAfternoon;
        TimeManager.Evening += GoEvening;
        TimeManager.Nightfall += GoHome;
    }

    void OnDisable()
    {
        TimeManager.Morning -= ResetPosition;                                           //Remove event listeners
        TimeManager.Afternoon -= GoAfternoon;
        TimeManager.Evening -= GoEvening;
        TimeManager.Nightfall -= GoHome;
    }

    void Update ()
    {
        if(!animator.GetBool("Talking"))                                                //If not talking
        {
            if (agent.remainingDistance > agent.stoppingDistance + 0.1f)                //If NPC has not yet reached destination
            {
                animator.SetBool("Moving", true);                                       //Play moving animation
            }
            else
            {
                animator.SetBool("Moving", false);                                      //Play idle animation
                if (target != null)                                                     //If NPC has a target, rotate to match target rotation
                {
                    transform.rotation = Quaternion.Lerp(transform.rotation, target.rotation, Time.deltaTime * 2f);
                }
            }
        }
    }

    public void GoToLocation(Transform targetTransform)                                 //Go to specified transform
    {
        target = targetTransform;
        agent.destination = target.position;
    }

    public void GoHome()                                                                //Go to home location
    {
        if (home != null)
        {
            GoToLocation(home);
        }
    }

    public void GoMorning()                                                             //Go to morning location
    {
        if(morning != null)
        {
            GoToLocation(morning);
        }
    }

    public void GoAfternoon()                                                           //Go to afternoon location
    {
        if (afternoon != null)
        {
            GoToLocation(afternoon);
        }
    }

    public void GoEvening()                                                             //Go to evening location
    {
        if (evening != null)
        {
            GoToLocation(evening);
        }
    }

    public void ResetPosition()                                                         //Reset position to home location
    {
        WarpHome();
        GoMorning();
    }

    public void WarpHome()
    {
        agent.Warp(home.transform.position);
        agent.destination = home.transform.position;
        transform.rotation = home.transform.rotation;
        target = home.transform;
    }

    public void StartTalking()                                                          
    {
        if (!enabled)                                                                   //Retrieve animator & agent again, because even if this code is moved to Start() it still doesn't always work
        {
            animator = GetComponent<Animator>();
            agent = GetComponent<NavMeshAgent>();
        }

        animator.SetBool("Talking", true);                                              //Trigger talking animation
        agent.destination = transform.position;                                         //Set destination to current position to stop NPC from walking away
        transform.LookAt(GameObject.FindGameObjectWithTag("Player").transform);         //Look at player while talking to them
    }

    public void StopTalking()
    {
        animator.SetBool("Talking", false);                                             //Return to idle animation

        if (enabled && target != null)                                                  //Continue travelling to destination if not already there
        {
            agent.destination = target.position;
        }

        StartCoroutine("DelayInteraction");
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Door")                                         //Open door when NPC enters trigger zone
        {
            hitDoor = collision.gameObject.GetComponent<Door>();
            StartCoroutine("OpenCloseDoor");
        }
    }

    IEnumerator OpenCloseDoor()                                                         //Start door opening, wait 0.5 seconds, then slam it closed to prevent player getting through
    {
        hitDoor.Open();
        yield return new WaitForSeconds(0.5f);
        hitDoor.Close();
    }

    IEnumerator DelayInteraction()
    {
        gameObject.tag = "Untagged";
        yield return new WaitForSeconds(10f);
        gameObject.tag = "Character";
    }
}
