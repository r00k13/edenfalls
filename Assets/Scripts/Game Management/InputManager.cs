using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

//Unifies all inputs into a set of static variables accessible by other scripts.

public class InputManager : MonoBehaviour
{
    public enum InputType {Mouse, XBOX, PS4, Steam, Unknown}                        //Enum for all possible input types
    public static InputType type = InputType.Mouse;

    public static float cameraX = 0;                                                //Static variables used by other scripts
    public static bool menu = false;
    public static bool submit = false;

    private float delayTime = 0f;                                                   //Delay timer, to avoid multiple button presses 

    private StandaloneInputModule inputModule;

    void Start()
    {
        inputModule = GetComponent<StandaloneInputModule>();

        if (Input.GetJoystickNames().Length > 0)                                    //If a controller is plugged in
        { 
            if (Input.GetJoystickNames()[0] == "Wireless Controller" || Input.GetJoystickNames()[0] == "Sony Computer Entertainment Wireless Controller" || Input.GetJoystickNames()[0] == "Sony Interactive Entertainment Wireless Controller")
            {
                SetInputMode(InputType.PS4);                                        //Set input type to PS4
                Debug.Log("PS4 controller detected");                               
            }
            else if (Input.GetJoystickNames()[0] == "Controller (XBOX 360 For Windows)" || Input.GetJoystickNames()[0] == "Controller (Xbox One For Windows)")
            {
                SetInputMode(InputType.XBOX);                                       //Set input type to XBOX
                Debug.Log("XBOX controller detected");
            }
            else
            {
                SetInputMode(InputType.Unknown);                                    //Set input type to unknown
                Debug.Log("Unknown controller detected: " + Input.GetJoystickNames()[0]);
            }
        }
        else
        {
            SetInputMode(InputType.Mouse);                                          //Set input type to mouse & keyboard
            Debug.Log("No controller detected");
        }
	}

    private void Update()
    {
        if(type == InputType.PS4)                                                   //If input type is PS4, get input from PS4 inputs
        {
            if (Input.GetButton("Menu PS4") && delayTime < Time.timeSinceLevelLoad)
            {
                menu = true;
                DelaySubmit(0.2f);
            }
            else
            {
                menu = false;
            }

            if (Input.GetButton("Submit PS4") && delayTime < Time.timeSinceLevelLoad)
            {
                submit = true;
                DelaySubmit(0.2f);
            }
            else
            {
                submit = false;
            }

            cameraX = Input.GetAxis("Camera X PS4") * 2;
        }
        else                                                                        //Otherwise, get input from normal inputs (XBOX & mouse & keyboard work the same, hopefully others would too)
        {
            if (Input.GetButton("Menu") && delayTime < Time.timeSinceLevelLoad)
            {
                menu = true;
                DelaySubmit(0.2f);
            }
            else
            {
                menu = false;
            }

            if ((Input.GetButton("Submit") || Input.GetMouseButton(0)) && delayTime < Time.timeSinceLevelLoad)
            {
                submit = true;
                DelaySubmit(0.2f);
            }
            else
            {
                submit = false;
            }

            if (type == InputType.XBOX && type == InputType.Unknown)                //If camera X is coming from an analogue stick, double it, as analogue stick values only range between -1 & 1
            {
                cameraX = Input.GetAxis("Camera X") * 2;
            }
            else
            {
                cameraX = Input.GetAxis("Camera X");
            }  
        }
    }

    public void SetInputMode(InputType i)
    {
        switch (i)                                          
        {
            case InputType.Mouse:
                type = InputType.Mouse;                                             //Set input type to mouse & keyboard
                inputModule.submitButton = "Submit";
                break;
            case InputType.XBOX:
                type = InputType.XBOX;                                              //Set input type to XBOX
                inputModule.submitButton = "Submit";
                break;
            case InputType.PS4:
                type = InputType.PS4;                                               //Set input type to PS4
                inputModule.submitButton = "Submit PS4";
                break;
            case InputType.Steam:
                type = InputType.Steam;                                             //Set input type to steam controller
                inputModule.submitButton = "Submit";
                break;
            case InputType.Unknown:
                type = InputType.Unknown;                                           //Set input type to unknown
                inputModule.submitButton = "Submit";
                break;
        }
    }

    public void DelaySubmit(float delay)                                                       //Prevent the script from receiving inputs for 1 second
    {
        delayTime = Time.timeSinceLevelLoad + delay;
    }
}
