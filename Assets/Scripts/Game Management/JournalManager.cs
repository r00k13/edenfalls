using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

//Controls the player�s journal, which acts as an in-game menu and also displays any notes they have collected. 
//Contains functions to show and hide the journal, move between pages, and choose note images from the Artifact Manager.

public class JournalManager : MonoBehaviour 
{
    public GameObject canvasJournal;                                                    //UI canvas for journal
    public GameObject menu, options, about;                                             //Game menu, options, & about pages
    public Image journalImageL, journalImageR;                                          //Left & right images from journal
    private int page = 0;                                                               //Current page (increments by 2)
    private int noOfPages;                                                              //Total number of pages (number of notes + 2)
    private AudioSource pageSound;
    public AudioClip[] pageSounds;

    private void Start()
    {
        pageSound = canvasJournal.GetComponent<AudioSource>();                          //Retrieve audio source
        canvasJournal.SetActive(false);                                                 //Hide journal
        noOfPages = 2 + GetComponent<ArtifactManager>().notes.Length;                   //Number of pages in journal is 2 (menu) + the number of notes in the game
    }

    void Update()
    {
        if (InputManager.menu)                                                          //Toggle journal canvas when player presses menu
        {
            if(!GameManager.paused)                                                     //If the game isn't paused
            {
                canvasJournal.SetActive(true);                                          //Show the journal canvas 
                page = 0;                                                               //Reset to page 0
                options.SetActive(false);                                               //Hide options & about pages
                about.SetActive(false);
                TurnPage(false);                                                        //Call turnpage() to re-enable menu
                GameManager.paused = true;                                              //Pause game
            }
            else if(canvasJournal.activeInHierarchy)                                    //If the game is paused & the journal canvas is active
            {
                Resume();                                                               //Hide journal & unpause
            }
        }
    }

    public void Resume()                                                                
    {
        canvasJournal.SetActive(false);                                                 //Hide journal
        GameManager.paused = false;                                                     //Unpause
    }

    public void Options()
    {
        options.SetActive(true);                                                        //Show the options page
        PlayPageSound();                                                                //Play a page turn sound
    }

    public void About()
    {
        about.SetActive(true);                                                          //Show the about page
        PlayPageSound();                                                                //Play a page turn sound
    }

    public void Survey()                                                                
    {
        Application.OpenURL("https://goo.gl/forms/h3Gbj4x2s1rHHaMA3");                  //Open the survey in a browser
    }

    public void QuitToMenu()
    {
        SceneManager.LoadScene(0);                                                      //Load main menu scene
    }

    public void TurnPage(bool forward)                                                  //Move forward or backward through journal
    {
        if(options.activeInHierarchy || about.activeInHierarchy)
        {
            menu.SetActive(true);
            options.SetActive(false);
            about.SetActive(false);
            journalImageL.enabled = false;
            journalImageR.enabled = false;
        }
        else
        {
            if (forward && page < noOfPages)                                            //If moving forward & not on last page
            {
                page += 2;                                                              //Increment page number by 2
            }
            else if (!forward && page > 0)                                              //If moving backward & not on first page
            {
                page -= 2;
            }

            if (page == 0)                                                              //If on first page, show menu & hide images
            {
                menu.SetActive(true);
                options.SetActive(false);
                about.SetActive(false);
                journalImageL.enabled = false;
                journalImageR.enabled = false;
            }
            else                                                                        //Otherwise, hide menu, & set images
            {
                menu.SetActive(false);

                ArtifactManager art = GetComponent<ArtifactManager>();
                SetImage(journalImageL, art.GetJournalPage(page - 2));                  //Set the left image
                SetImage(journalImageR, art.GetJournalPage(page - 1));                  //Set the right image
            }
        }
        PlayPageSound();                                                                //Play a page turn sound
    }

    void SetImage(Image img, Sprite page)
    {
        img.sprite = page;                                                              //Attempt to assign sprite to image
        if (page != null)                                                               //If getjournalpage() returned a sprite, show the image
        {
            img.enabled = true;
            img.preserveAspect = true;
        }
        else                                                                            //Otherwise, hide the image
        {
            img.enabled = false;
        }
    }

    private void PlayPageSound()
    {
        pageSound.clip = pageSounds[Random.Range(0, pageSounds.Length)];                //Pick a random clip from the array & assign it to the audio source
        pageSound.Play();                                                               //Play page sound
    }
}
