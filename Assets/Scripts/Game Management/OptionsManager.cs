﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityStandardAssets.ImageEffects;

//Manages, saves, & applies the game options chosen by the player.

public class OptionsManager : MonoBehaviour
{
    GameOptions options = new GameOptions();                            //Object used to store options data

    public Slider volMusic, volSFX;                                     //UI elements
    public Dropdown resolution;
    public Toggle fullscreen, aA;
    public Dropdown inputType;

    void Start()
    {
        LoadOptions();
        SetOptionsUI();                                                 //Set the UI elements to match options data
        ApplyOptions();
    }

    void OnEnable()                                                     //Every time options menu is enabled, reset UI elements to current data
    {
        SetOptionsUI();
    }

    public void ApplyOptions()                                          //Apply the effects of the options chosen
    {
        SoundManager.musicMultiplier = options.volMusic;
        SoundManager.sFXMultiplier = options.volSFX;

        Screen.SetResolution(options.resolutionX, options.resolutionY, options.fullscreen);
        Camera.main.GetComponent<Antialiasing>().enabled = options.aA;

        switch (inputType.value)                                        //Set the desired input from the dropdown
        {
            case 0:
                GetComponent<InputManager>().SetInputMode(InputManager.InputType.Mouse);
                break;
            case 1:
                GetComponent<InputManager>().SetInputMode(InputManager.InputType.XBOX);
                break;
            case 2:
                GetComponent<InputManager>().SetInputMode(InputManager.InputType.PS4);
                break;
            case 3:
                GetComponent<InputManager>().SetInputMode(InputManager.InputType.Steam);
                break;
            case 4:
                GetComponent<InputManager>().SetInputMode(InputManager.InputType.Unknown);
                break;
        }
    }


    void SetOptionsUI()                                                 //Set the values of the UI elements
    {
        volMusic.value = options.volMusic;
        volSFX.value = options.volSFX;

        switch (options.resolutionX)                                    
        {
            case 1280:
                resolution.value = 0;
                break;
            case 1366:
                resolution.value = 1;
                break;
            case 1920:
                resolution.value = 2;
                break;
            default:
                resolution.value = 3;
                    break;
        }

        fullscreen.isOn = options.fullscreen;
        aA.isOn = options.aA;

        switch (InputManager.type)                                       //Set the Input Type dropdown based on what the current input type is
        {
            case InputManager.InputType.Mouse:
                inputType.value = 0;
                break;
            case InputManager.InputType.XBOX:
                inputType.value = 1;
                break;
            case InputManager.InputType.PS4:
                inputType.value = 2;
                break;
            case InputManager.InputType.Steam:
                inputType.value = 3;
                break;
            case InputManager.InputType.Unknown:
                inputType.value = 3;
                break;
        }
    }

    void GetOptionsUI()                                                 //Retrieve values from UI elements & store them in the options object
    {
        options.volMusic = volMusic.value;
        options.volSFX = volSFX.value;

        switch (resolution.value)                                        
        {
            case 0:
                options.resolutionX = 1280;
                options.resolutionY = 720;
                break;
            case 1:
                options.resolutionX = 1366;
                options.resolutionY = 768;
                break;
            case 2:
                options.resolutionX = 1920;
                options.resolutionY = 1080;
                break;
            case 3:
                options.resolutionX = Screen.width;
                options.resolutionY = Screen.height;
                break;
        }

        options.fullscreen = fullscreen.isOn;
        options.aA = aA.isOn;
    }

    public void SaveOptions()                                           //Save the options object to a JSON file
    {
        GetOptionsUI();                                                 //Ensure the data in the options object is up to date first
        string jsonText = JsonUtility.ToJson(options);                  //Convert data to JSON
        File.WriteAllText(Application.persistentDataPath + "/" + "options.json", jsonText);             //Write JSON to file
        ApplyOptions();
    }

    public void LoadOptions()                                           //Attempt to load data from file & store in the options object
    {
        string file = Application.persistentDataPath + "/" + "options.json";
        if (File.Exists(file))                                          //If the file can be found
        {
            string jsonFile = File.ReadAllText(file);                   //Read the file & store data as a string

            options = new GameOptions();                                //Reset the options object
            options = JsonUtility.FromJson<GameOptions>(jsonFile);      //Parse the JSON string 
        }
        else                                                            //If file is not found
        {
            Debug.Log("FILE NOT FOUND: " + file);

            options.volMusic = 0.8f;                                    //Set default values
            options.volSFX = 0.8f;
            options.aA = true;
        }

        options.resolutionX = Screen.width;                             //Set graphics options to whatever was set in launcher
        options.resolutionY = Screen.height;
        options.fullscreen = Screen.fullScreen;
    }
}

public struct GameOptions                                               //Struct with variables for each option presented to the user
{
    public float volMusic;
    public float volSFX;
    public int resolutionX;
    public int resolutionY;
    public bool fullscreen;
    public bool aA;
}