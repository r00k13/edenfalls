using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Fungus;

//Interfaces with Fungus, triggering the appropriate blocks in the flowchart and pausing the game while they play. Also sets and gets variables in the flowchart for use by other scripts.

public class FungusManager : MonoBehaviour
{
    public Flowchart flowchart;
    public Image dialogueBox;
    public Sprite normal, torn;
    public ButtonPrompts buttonPrompts;

    private void Start()
    {
        flowchart.ExecuteBlock("FadeIn");                                                   //Fade scene in from black at start
    }

    public void StartChat(string person)                                                    //Play the dialogue for a specific person at the current game stage
    {
        if (flowchart.FindBlock(person + GameManager.gameStage) != null)
        {
            GameManager.paused = true;
            buttonPrompts.Hide();
            flowchart.ExecuteBlock(person + GameManager.gameStage);
        }
        else
        {
            Debug.Log("Block " + person + GameManager.gameStage + " does not exist");
        }
    }

    public void StartChat(string person, int chatID)                                        //Play a specific dialogue
    {
        if(flowchart.FindBlock(person + chatID) != null)
        {
            GameManager.paused = true;
            buttonPrompts.Hide();
            flowchart.ExecuteBlock(person + chatID);
        }
        else
        {
            Debug.Log("Block " + person + chatID + " does not exist");
        }
    }

    public void Resume()                                                                    //Called from the flowchart whenever a dialogue ends. Unpauses the game
    {
        GetComponent<InputManager>().DelaySubmit(2f);                                       //Prevent any input for 2 seconds, to keep the player from restarting conversation if they button mashed through it
        GameManager.paused = false;
    }

    public void SetFlowchartBool(string key, bool value)                                    //Set a bool in the flowchart
    {
        flowchart.SetBooleanVariable(key, value);
    }

    public bool GetFlowchartBool(string key)                                                //Get a bool from the flowchart
    {
        return flowchart.GetBooleanVariable(key);
    }

    public void SetDialogueBox(bool ripped)                                                 //Set the sprite used on the dialogue box
    {
        if(ripped)
        {
            dialogueBox.sprite = torn;
        }
        else
        {
            dialogueBox.sprite = normal;
        }
    }
}
