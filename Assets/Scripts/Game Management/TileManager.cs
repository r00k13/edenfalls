using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Contains arrays of all the tiles that make up the forest. Randomly replaces tiles in each array at regular intervals.

public class TileManager : MonoBehaviour
{
    public Transform[] tilesMiddle, tilesOuter;                     //All tiles in the map
    public GameObject[] tilePrefabs;
    private float counter = 0;                                      //Counter to track time since last change
    
    void OnEnable()                                                 //Reset counter to 0 on enable
    {
        counter = 0;
    } 

	void Update ()
    {
        counter += Time.deltaTime;                                  //Increase counter

        if (counter > 5f)                                           //If 5 seconds have passed 
        {
            int random = 0;
            counter = 0;                                            //Reset counter to 0
            
            if (GameManager.gameStage >= 7)
            {
                random = Random.Range(0, 4);
            }

            if(random == 0)
            {
                SwapRandomTile(tilesMiddle);                        //Randomly swap a tile from the middle ring
            }
            else
            {
                SwapRandomTile(tilesOuter);                         //Randomly swap two tiles from the outer ring
            }
        }
    }

    void SwapRandomTile(Transform[] tiles)
    {
        int tileID = Random.Range(0, tiles.Length);                 //Pick a random tile
        if (!tiles[tileID].GetComponent<Tile>().locked && Vector3.Distance(tiles[tileID].transform.position, GameObject.FindGameObjectWithTag("Player").transform.position) > 100f)     //Make sure tile isn't locked or the one the player is on
        {
            GameObject newTile = Instantiate(tilePrefabs[Random.Range(0, tilePrefabs.Length)], tiles[tileID].transform.position, tiles[tileID].transform.rotation);
            newTile.transform.localEulerAngles = new Vector3(0, 90f * Random.Range(0, 4), 0);     //Rotate tile by a random multiple of 90
            Destroy(tiles[tileID].gameObject);
            tiles[tileID] = newTile.transform;
        }
        else                                                        //If player is on the tile, call method to try again
        {
            SwapRandomTile(tiles);
        }
    }

    public void QuickScramble()                                     //Instantly scramble the tiles
    {
        if (GameManager.gameStage >= 5)                             //If GameStage is at least 5 (meaning that middle ring is active)
        {
            for(int i = 0; i < 8; i++)                              //Randomly swap tiles 8 times from the middle ring
            {
                SwapRandomTile(tilesMiddle);
            }
        }
        if (GameManager.gameStage >= 8)                             //If GameStage is at least 8 (meaning that outer ring is active)
        {
            for (int i = 0; i < 12; i++)                            //Randomly swap tiles 12 times from the outer ring
            {
                SwapRandomTile(tilesMiddle);
            }
        }
    }
}
