using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Displays notes and artifacts when the player finds them, and contains a list of all notes and artifacts that have been found. Also displays special notes used for certain puzzles.

public class ArtifactManager : MonoBehaviour
{
    public Sprite[] notes;                                                  //Array of note images
    private bool[] foundNotes;                                              //Array of which notes the player has found
    public GameObject noteImage;                                            //UI image notes are shown on
    public ButtonPrompts buttonPrompts;

    private float noteDisplayTime = 0f;                                     //Delay timer to prevent player from immediately closing note

    void Update ()
    {
		if(noteImage.activeInHierarchy && GameManager.paused && InputManager.submit && Time.timeSinceLevelLoad > noteDisplayTime + 1f)          //If game is paused & player presses submit, hide note & resume play
        {
            GameManager.paused = false;
            noteImage.SetActive(false);
            buttonPrompts.Hide();
        }
    }

    public void ShowNote(int i)                                             //Display a collectable note                                         
    {
        if(!GameManager.paused)
        {
            StartCoroutine("ShowButtonPrompts");                            //Show button prompts after a brief delay, so deleting the note in the world deosn't hide them again
        }
        GameManager.paused = true;                                          //Pause game
        noteImage.GetComponent<Image>().sprite = notes[i];                  //Set image sprite to the appropriate note image
        noteImage.SetActive(true);                                          //Show image
        noteDisplayTime = Time.timeSinceLevelLoad;

        foundNotes[i] = true;                                               //Mark note as found
    }

    public void ShowSpecialNote(Sprite s)                                   //Display a special note for a puzzle
    {
        if (!GameManager.paused)
        {
            StartCoroutine("ShowButtonPrompts");                            //Show button prompts after a brief delay, so deleting the note in the world deosn't hide them again
        }
        GameManager.paused = true;                                          //Pause game
        noteImage.GetComponent<Image>().sprite = s;                         //Set image sprite to the appropriate note image
        noteImage.SetActive(true);                                          //Show image
        noteDisplayTime = Time.timeSinceLevelLoad;
    }

    public Sprite GetJournalPage(int pageNo)                                //Return image for journal page if it has been found
    {
        if (pageNo < notes.Length && foundNotes[pageNo] && notes[pageNo] != null)   //If image exists & has been found, return it
        {
            return notes[pageNo];                                                   
        }
        else                                                                //Otherwise, return nothing
        {
            return null;
        }
    }

    private IEnumerator ShowButtonPrompts()
    {
        yield return new WaitForSeconds(0.8f);
        buttonPrompts.Show();                                               //Show button prompts
    }

    public bool[] GetFoundNotes()                                           //Return array of found notes
    {
        return foundNotes;
    }

    public void SetFoundNotes(bool[] fN)
    {
        if(fN.Length > 0)                                                   //If an array of found notes was in the save data, assign it
        {
            foundNotes = fN;
        }
        else                                                                //Otherwise, create a new array with all entries set to false
        {
            foundNotes = new bool[notes.Length];
            for (int i = 0; i < foundNotes.Length; i++)
            {
                foundNotes[i] = false;
            }
        }
    }
}
