using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;

//Tracks the player�s progress in the game, alters the game world to fit the current stage, and saves and loads the game using JSON files.

public class GameManager : MonoBehaviour
{
    public static bool paused = false;                                      //Static variable used to track when game is paused. Used instead of Time.timeScale, as that interferes with Fungus
    public static int gameStage = 1;                                        //Tracks player's progress, increases by one every time they make a significant advance

    public GameObject tileTown;
    public GameObject[] tileLayers;                                         //Sets of tiles & walls that unlock as the game progresses
    public GameObject[] wallLayers;

    private Puzzle[] puzzles;                                               //Puzzle scripts on player

    private SaveData saveData;                                              //SaveData object

    public GameObject saveIcon;

    void Start()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");     //Retrieve puzzle scripts from player
        puzzles = new Puzzle[5];
        puzzles[0] = player.GetComponent<TornNote>();
        puzzles[1] = player.GetComponent<BrokenBridge>();
        puzzles[2] = player.GetComponent<OtisHouse>();
        puzzles[3] = player.GetComponent<FindDarnex>();
        puzzles[4] = player.GetComponent<FixDeer>();

        gameStage = 1;                                                      //Reset gameStage in case game has already been played this session

        Load();                                                             //Attempt to load a game
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.K))                                     //Skip a stage when player presses K, used for testing purposes
        {
            SkipStage();
            Debug.Log("Stage " + gameStage + " started");
            gameStage--;                                                    //Reduce stage by one to cancel out effect of calling NextStage()
            NextStage();
        }
    }

    public void NextStage()                                                 //Increase game stage, & activate puzzles or expand game world if appropriate
    {
        gameStage++;

        switch(gameStage)
        {
            case 3:
                puzzles[1].enabled = true;
                break;
            case 5:
                ExpandMap(1);                                               //Enable middle ring of tiles
                puzzles[2].enabled = true;
                break;
            case 6:
                puzzles[3].enabled = true;
                break;
            case 7:
                puzzles[4].enabled = true;
                ExpandMap(2);                                               //Enable outer ring of tiles
                break;
            case 8:
                puzzles[4].enabled = true;
                puzzles[4].SetStage(0, new bool[0]);                        //Set first part of mural puzzle complete
                break;
            case 9:
                puzzles[4].enabled = true;
                puzzles[4].SetStage(0, new bool[0]);                        //Set second part of mural puzzle complete
                break;
            case 10:
                StartCoroutine("EndTransition");
                break;
        }
    }

    private void SkipStage()                                                //Increase game stage, & automatically complete any relevant puzzle
    {
        gameStage++;
        
        switch (gameStage)
        {
            case 3:
                if (puzzles[0] != null)
                {
                    puzzles[0].AutoComplete();
                }
                break;
            case 4:
                if (puzzles[1] != null)
                {
                    puzzles[1].AutoComplete();
                }
                break;
            case 5:
                ExpandMap(1);
                break;
            case 6:
                if (puzzles[2] != null)
                {
                    puzzles[2].AutoComplete();
                }
                break;
            case 7:
                if (puzzles[3] != null)
                {
                    puzzles[3].AutoComplete();
                }
                ExpandMap(2);
                break;
        }
    }

    private IEnumerator EndTransition()                                     //Set up effects & transition to ending scene after 3 seconds
    {
        tileTown.tag = "Creepy";                                            //Make town creepy to trigger camera glitch & chromatic aberration
        GetComponent<FungusManager>().StartChat("FadeOut", 0);              //Fade to black
        yield return new WaitForSeconds(3f);                                //Wait 3 seconds while fade happens
        SceneManager.LoadScene(2);                                          //Load ending scene
    }

    private void ExpandMap(int mapStage)                                    //Enable & disable tiles & walls to increase the world size by 1 ring of tiles
    {
        wallLayers[mapStage - 1].SetActive(false);
        wallLayers[mapStage].SetActive(true);
        tileLayers[mapStage].SetActive(true);
    }

    public void NewDay()                                                    //Start a new day
    {
        TimeManager.clock = 6.99f;

        if (gameStage == 4)                                                 //If game stage is 4, increase by 1 to start the Otis House puzzle
        {
            NextStage();
        }

        GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerManager>().GoHome();      //Move player to home location
    }

    public void RestartDay()                                                //Restart current day (Not used any more)
    {
        SceneManager.LoadScene(1);
    }

    public void Save()
    {
        saveData = new SaveData();                                          //Create save data & assign variables
        saveData.gameStage = gameStage;
        saveData.foundNotes = GetComponent<ArtifactManager>().GetFoundNotes();

        if (gameStage < 3)
        {
            saveData.puzzleStage = puzzles[0].GetStage();
            saveData.puzzleCollected = puzzles[0].GetCollected();
        }
        else if (gameStage < 4)
        {
            saveData.puzzleStage = puzzles[1].GetStage();
            saveData.puzzleCollected = puzzles[1].GetCollected();
        }
        else if (gameStage < 6)
        {
            saveData.puzzleStage = puzzles[2].GetStage();
            saveData.puzzleCollected = puzzles[2].GetCollected();
        }
        else if (gameStage < 7)
        {
            saveData.puzzleStage = puzzles[3].GetStage();
            saveData.puzzleCollected = puzzles[3].GetCollected();
        }
        else
        {
            saveData.puzzleStage = puzzles[4].GetStage();
            saveData.puzzleCollected = puzzles[4].GetCollected();
        }

        saveIcon.SetActive(true);
        string jsonText = JsonUtility.ToJson(saveData);                     //Convert save data to JSON format
        File.WriteAllText(Application.persistentDataPath + "/" + "save.json", jsonText);        //Save JSON in file
    }

    private void Load()
    {
        string file = Application.persistentDataPath + "/" + "save.json";

        if (File.Exists(file))                                              //Attempt to load file    
        {
            string jsonFile = File.ReadAllText(file);                       //Read JSON from file

            saveData = new SaveData();                                      //Create save data & assign values from JSON to variables
            saveData = JsonUtility.FromJson<SaveData>(jsonFile);

            for (int i = 1; i < saveData.gameStage; i++)                    //Auto-complete puzzles until reaching the correct stage 
            {
                SkipStage();
            }
            gameStage--;                                                    //Reduce stage by one to cancel out effect of calling NextStage()
            NextStage();

            GetComponent<ArtifactManager>().SetFoundNotes(saveData.foundNotes);     //Set found notes array in ArtifactManager

            if (gameStage < 3)                                              //Assign puzzle data 
            {
                puzzles[0].SetStage(saveData.puzzleStage, saveData.puzzleCollected);
            }
            else if (gameStage < 4)
            {
                puzzles[1].SetStage(saveData.puzzleStage, saveData.puzzleCollected);
            }
            else if (gameStage < 6)
            {
                puzzles[2].SetStage(saveData.puzzleStage, saveData.puzzleCollected);
            }
            else if (gameStage < 7)
            {
                puzzles[3].SetStage(saveData.puzzleStage, saveData.puzzleCollected);
            }
            else
            {
                puzzles[4].SetStage(saveData.puzzleStage, saveData.puzzleCollected);
            }

            Debug.Log("Loaded save at game stage " + saveData.gameStage);

            if (saveData.gameStage > 1)                                     //If player had made any progress, start game from a new day
            {
                NewDay();
            }
            else                                                            //Otherwise, start game from beginning
            {
                GetComponent<ArtifactManager>().ShowNote(0);
                TimeManager.clock = 11f;
            } 
        }
        else                                                                //If no save data was found, start game from beginning
        {
            Debug.Log("FILE NOT FOUND: " + file);
            GetComponent<ArtifactManager>().ShowNote(0);
            TimeManager.clock = 11f;
        }
    }
}

public struct SaveData
{
    public int gameStage;                                                   //Stage the player is on
    public bool[] foundNotes;                                               //Which notes they've found
    public int puzzleStage;                                                 //How many items they have or tasks they've completed for the current puzzle
    public bool[] puzzleCollected;                                          //Which items they've collected for the current puzzle
}
