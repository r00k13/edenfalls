using LPWAsset;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Listens for the Morning and Nightfall events, and sets the light source for the water it is attached to to be either the sun or the moon.

public class WaterLighting : MonoBehaviour
{
    public Light sun, moon;

    void OnEnable()
    {
        TimeManager.Nightfall += SetMoon;                   //Add event listener that calls SetMoon() when nightfall event happens
        TimeManager.Morning += SetSun;
    }

    void OnDisable()
    {
        TimeManager.Nightfall -= SetMoon;                   //Remove event listeners
        TimeManager.Morning -= SetSun;
    }

    void SetSun()                                           //Sets the sun as the sun of the water script
    {
        GetComponent<LowPolyWaterScript>().sun = sun;
    }

    void SetMoon()                                          //Sets the moon as the sun of the water script
    {
        GetComponent<LowPolyWaterScript>().sun = moon;
    }
}
