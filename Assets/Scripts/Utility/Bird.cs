using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bird : MonoBehaviour
{
    public float speed = 20f;                                                               //How fast the bird moves
    private Transform player;
    
    void Start ()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;                      //Find player
	}
	
	void Update ()
    {
        if(!GameManager.paused)
        {
            transform.Translate(Vector3.forward * speed * Time.deltaTime);                  //Move forward at current speed

            if (Vector3.Distance(transform.position, player.position) > 50f)                //If bird is too far away from player
            {
                Move();                                                                     //Relocate to new position
            }
        }
    }

    private void Move()
    {
        transform.position = new Vector3(player.position.x, 8, player.position.z - 40f);    //Move to a point behind the player
        transform.eulerAngles = Vector3.zero;                                               //Rotate to face player
        transform.RotateAround(player.position, Vector3.up, Random.Range(0, 360f));         //Rotate random angle around player
    }

    //private void OnCollisionEnter(Collision collision)
    //{
    //    Move();
    //}
}
