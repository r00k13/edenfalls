using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Moves the train towards the right side of the map, and resets its position to the leftmost side of the map when it gets there. Train waits for 3 seconds, then accelerates to full speed

public class Train : MonoBehaviour
{
    public float speed = 20f;                                                       //Max speed
    private float currentSpeed = 0f;                                                //Current speed

	void Update ()
    {
        if(Time.timeSinceLevelLoad > 5f && !GameManager.paused)                     //Once train has been waiting for 3 seconds
        {
            if(currentSpeed < speed)                                                //If current speed is leass than max, speed up
            {
                currentSpeed += Time.deltaTime * 4;
            }

            transform.Translate(Vector3.forward * currentSpeed * Time.deltaTime);   //Move forward at current speed

            if (transform.position.x > 300f)                                        //If train is off the right side of the map, jump to the left side of the map
            {
                transform.position = new Vector3(-300f, transform.position.y, transform.position.z);
            }
        }
	}
}
