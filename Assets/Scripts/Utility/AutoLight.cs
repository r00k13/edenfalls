using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoLight : MonoBehaviour
{
    public GameObject lightSource;

    void OnEnable()
    {
        TimeManager.Nightfall += ActivateLight;                     //Add event listener that calls activatelight() when nightfall event happens
        TimeManager.Morning += DeactivateLight;
    }

    void OnDisable()
    {
        TimeManager.Nightfall -= ActivateLight;                     //Remove event listener
        TimeManager.Morning -= DeactivateLight;
    }

    void ActivateLight()                                            //Enables the object set as the light source
    {
        lightSource.SetActive(true);
    }

    void DeactivateLight()                                          //Disables the object set as the light source
    {
        lightSource.SetActive(false);
    }
}
