using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Rotates the object 90 degrees when the Open() function is called, assuming the locked boolean isn't set to true.

public class Door : MonoBehaviour
{
    public bool locked = false;                             //Is the door locked?
    public bool axisLeft = true;                            //Is the door's axis to the left or the right when seen from outside?
    private bool open = false;
    private AudioSource audioSource;
    private Quaternion closedRotation;

	void Start ()
    {
        audioSource = GetComponent<AudioSource>();
        if (axisLeft)                                       //If the door's axis is to the left, change its y rotation from 0 to 359 to avoid issues with negative values
        {
            transform.localRotation = Quaternion.Euler(new Vector3(0, 359f, 0));
        }

        closedRotation = transform.localRotation;
        TimeManager.Morning += Close;                       //Add event listener

        enabled = false;                                    //Disable script until it is needed
    }

    private void OnDestroy()
    {
        TimeManager.Morning -= Close;                       //Add event listener
    }

    void Update ()
    {
		if(open)                                            //If door has been opened but it is not fully open, rotate it
        {
            if(axisLeft && transform.localEulerAngles.y > 270f)
            {
                transform.Rotate(new Vector3(0, -100f * Time.deltaTime, 0));
            }
            else if (transform.localEulerAngles.y < 90f)
            {
                transform.Rotate(new Vector3(0, 100f * Time.deltaTime, 0));
            }
        }
	}

    public void Open()                                      //Remove "Door" tag so player can't interact with it any more, then open door
    {
        gameObject.tag = "Untagged";
        open = true;
        enabled = true;
        audioSource.Play();                                 //Play open door sound
    }

    public void Close()                                     //Reset door
    {
        if(gameObject.tag != "PuzzleDoor")
        {
            gameObject.tag = "Door";
        }
        open = false;
        enabled = false;
        transform.localRotation = closedRotation;
    }
}
