using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//A boolean on this script states whether it is locked, preventing the tile manager from replacing the tile it is on. Used for tiles that are needed for puzzles.

public class Tile : MonoBehaviour
{
    public bool locked = false;
}
