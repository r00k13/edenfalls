using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Turns the object to face the player, and fades the alpha value of any materials on it as the player gets closer, destroying the object entirely when the player gets very close.

public class Watcher : MonoBehaviour
{
    private Renderer render;

	void Start ()
    {
        render = GetComponentInChildren<Renderer>();                                                        //Retrieve the renderer from the mesh
	}

	void Update ()
    {
        Transform player = GameObject.FindGameObjectWithTag("Player").transform;                            //Find the player
        transform.LookAt(new Vector3(player.position.x, transform.position.y, player.position.z));          //Turn to face the player

        float alpha = (Vector3.Distance(player.position, transform.position) / 20f) - 0.1f;                 //Determine alpha value based on distance from player
        alpha = Mathf.Clamp(alpha, 0, 0.8f);
        if(alpha < 0.05f)                                                                                   //If alpha is almost 0, destroy watcher
        {
            Destroy(gameObject);
        }

        Color c = render.materials[0].color;                                                                //Create new colour from colour of first material & new alpha value
        c.a = alpha;

        for(int i = 0; i < render.materials.Length; i++)                                                    //Assign colour to every material on renderer
        {
            render.materials[i].color = c;
        }

    }
}
