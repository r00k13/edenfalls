using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Script attached to notes, used to identify note type

public class Artifact : MonoBehaviour
{
    public int artifactID;                      //Identifier for note, passed to notemanager to decide which one to show
    public bool puzzle = false;                 //Is the note part of the puzzle system?

    private void Start()
    {
        if (!puzzle)                            //If the note is not part of the puzzle system, check if it has already been collected
        {
            StartCoroutine("CheckCollected");
        }
    }

    IEnumerator CheckCollected()
    {
        yield return new WaitForSeconds(1f);

        ArtifactManager aM = GameObject.FindGameObjectWithTag("Manager").GetComponent<ArtifactManager>();

        if (aM.GetFoundNotes()[artifactID])     //If this note has already been found, destroy itself
        {
            Destroy(gameObject);
        }
    }
}
